from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    html = 'lab_6.html'
    return render(request, html)

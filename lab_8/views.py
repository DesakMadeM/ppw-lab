from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    html ='lab_8/lab_8.html'
    return render(request, html)

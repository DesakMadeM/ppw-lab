var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } else if (x === 'log') {
    print.value = Math.round(Math.log10(print.value) * 10000) / 10000;
  } else if (x === 'sin') {
    print.value = Math.round(Math.sin(print.value * Math.PI / 180) * 10000) / 10000;
  } else if (x === 'tan') {
    print.value = Math.round(Math.tan(print.value * Math.PI / 180) * 10000) / 10000;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//define function
//this function takes a theme object as a parameter
function applyTheme(theme){
  //modify the css based on selector
  $('body').css('background', theme.bcgColor);
  $('html').css('color', theme.fontColor);
}

if (typeof(Storage) !== "undefined") {
  if (localStorage.getItem("themes") === null || localStorage.getItem("selectedTheme") === null ) {
    //inisiasi data JSON themes
    localStorage.themes = `[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ]`
    //JSON.stringify() is to create a JSON string out of an object/array
    //inisiasi selectedTheme (default theme nya)
    localStorage.selectedTheme = JSON.stringify({"bcgColor":"#3F51B5","fontColor":"#FAFAFA"});
  }

  //JSON.parse() will convert a JSON string into a JS object
  //applying default theme
  applyTheme(JSON.parse(localStorage.selectedTheme));
} else {
  alert("Your browser doesn't support web storage!");
}

$(document).ready(() => {

  //apply default theme when the website is loaded for the first time
  applyTheme(JSON.parse(localStorage.selectedTheme));

  $("img").on('click', () => {
    $('.chat-body').toggle();
  });

  $('.chat-text').on('keypress', (event) =>{
    if(event.which ==13){
        var text = $('textarea').val();
        $('textarea').val("");
        $('.msg-insert').append('<p class="msg-send">'+text+ '</p>');
        $('.msg-insert').append('<p class="msg-receive">'+"heheh"+ '</p>');

    }
  });

  /*Select2 can render programmatically supplied data from an
  array or remote data source (AJAX) as dropdown options.*/
  $('.my-select').select2({
     'data': JSON.parse(localStorage.themes)
   });


   $('.apply-button').on('click', () => {
     //get theme id (index of array element)
     var themeid = parseInt($('.my-select').select2('data')[0].id);
     //convert object to string
     localStorage.selectedTheme = JSON.stringify(JSON.parse(localStorage.themes)[themeid]);
     //apply theme based on selected theme(berupa object), call defined function applyTheme
     applyTheme(JSON.parse(localStorage.selectedTheme));
   });

});

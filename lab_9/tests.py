import lab_9.csui_helper as csui_helper
import lab_9.custom_auth as custom_auth
import lab_9.api_enterkomputer as api_enterkomputer

from django.test.utils import override_settings
from django.conf import settings
from django.test import TestCase, Client, RequestFactory
from django.urls import resolve, reverse

from http.cookies import SimpleCookie
from .views import index, COOKIE_USERNAME, COOKIE_PASSWORD

from mock import patch
def set_logged_in_session(session):
	session['user_login'] = 'kodok'
	session['access_token'] = 'sombong'
	session['kode_identitas'] = 1010101010
	session['role'] = 'mahasisa'
	session.save()

def set_logged_in_cookie(client):
	client.cookies = SimpleCookie({
		'user_login': COOKIE_USERNAME,
		'user_password': COOKIE_PASSWORD,
	})

def mocked_access_token_response(*args, **kwargs):
	class MockResponse:
		def __init__(self, json_data):
			self.json_data = json_data

		def json(self):
			return self.json_data

	return MockResponse({
		'access_token': "MOCKTOKEN"
	})


def mocked_fail_access_token_request(*args, **kwargs):
	raise Exception()

class Lab9UnitTest(TestCase):
	def setUp(self):
		super(Lab9UnitTest, self).setUp()
		patcher_custom_auth_get_access_token = patch('lab_9.custom_auth.get_access_token', return_value='MOCKED')
		patcher_custom_auth_get_access_token.start()
		self.addCleanup(patcher_custom_auth_get_access_token.stop)

		patcher_custom_auth_verify_user = patch('lab_9.custom_auth.verify_user', return_value={'identity_number': '123213','role': 'MOCKTEST'})

		patcher_custom_auth_verify_user.start()
		self.addCleanup(patcher_custom_auth_verify_user.stop)

		patcher_requests_get = patch('requests.get', side_effect=mocked_access_token_response)
		patcher_requests_get.start()
		self.addCleanup(patcher_requests_get.stop)

		patcher_requests_request = patch('requests.request', side_effect=mocked_access_token_response)
		patcher_requests_request.start()
		self.addCleanup(patcher_requests_request.stop)

	def test_lab_9_url_exist(self):
		response = Client().get(reverse('lab-9:index'))
		self.assertEquals(response.status_code, 200)

	def test_lab_9_using_index_func(self):
		found = resolve(reverse('lab-9:index'))
		self.assertEquals(found.func, index)

	def test_lab_9_index_not_logged_in_uses_login_template(self):
		response = Client().get(reverse('lab-9:index'))
		self.assertTemplateUsed(response, 'lab_9/session/login.html')

	def test_lab_9_index_logged_in_redirect_profile(self):
		client = Client()
		set_logged_in_session(client.session)
		response = client.get(reverse('lab-9:index'))
		self.assertRedirects(response, reverse('lab-9:profile'))

	def test_lab_9_profile_not_logged_in_redirect_index(self):
		client = Client()
		response = client.get(reverse('lab-9:profile'))
		self.assertRedirects(response, reverse('lab-9:index'))

	def test_lab_9_add_session_item(self):
		client = Client()
		parameters = {
			'category':'drone',
			'id':'123'
		}
		client.post(reverse('lab-9:add_session_item'), parameters)

		parameters = {
			'category':'drone',
			'id':'1234'
		}
		client.post(reverse('lab-9:add_session_item'), parameters)

		self.assertTrue('123' in client.session['drone'])
		self.assertTrue('1234' in client.session['drone'])

	def test_lab_9_del_session_item(self):
		client = Client()
		parameters = {
			'category':'drone',
			'id':'1234'
		}
		client.post(reverse('lab-9:add_session_item'), parameters)

		client.post(reverse('lab-9:del_session_item'), parameters)
		self.assertTrue('1234' not in client.session['drone'])

	def test_lab_9_clear_session_item(self):
		client = Client()
		parameters = {
			'category':'drone',
			'id':'1234'
		}
		client.post(reverse('lab-9:add_session_item'), parameters)

		response = client.get(reverse('lab-9:clear_session_item', args=('drone',)))
		self.assertTrue('drone' not in response.client.session)

	def test_lab_9_cookie_login_use_login_template(self):
		client = Client()
		response = client.get(reverse('lab-9:cookie_login'))
		self.assertTemplateUsed(response, 'lab_9/cookie/login.html')

	def test_lab_9_cookie_logged_in_redirect_to_profile(self):
		client = Client()
		set_logged_in_cookie(client)
		response = client.get(reverse('lab-9:cookie_login'))
		self.assertRedirects(response, reverse('lab-9:cookie_profile'))

	def test_lab_9_cookie_auth_login_not_POST(self):
		response = Client().get(reverse('lab-9:cookie_auth_login'))
		self.assertRedirects(response, reverse('lab-9:cookie_login'))

	def test_lab_9_cookie_auth_login_success(self):
		client = Client()
		parameters = {
			'username': COOKIE_USERNAME,
			'password': COOKIE_PASSWORD,
		}
		response = client.post(reverse('lab-9:cookie_auth_login'), parameters)
		self.assertTrue('user_login' in response.client.cookies.keys())
		self.assertTrue('user_password' in response.client.cookies.keys())

	def test_lab_9_cookie_auth_login_fail(self):
		parameters = {
			'username': 'hahahahahahah',
			'password': 'huhuhuhuhuhuhu',
		}
		response = Client().post(reverse('lab-9:cookie_auth_login'), parameters)
		self.assertTrue('user_login' not in response.client.cookies.keys())
		self.assertTrue('user_password' not in response.client.cookies.keys())

	def test_lab_9_cookie_profile_not_logged_in(self):
		response = Client().get(reverse('lab-9:cookie_profile'))
		self.assertRedirects(response, reverse('lab-9:cookie_login'))

	def test_lab_9_cookie_profile_fake_logged_in_uses_login_template(self):
		client = Client()

		client.cookies = SimpleCookie({
			'user_login': 'hahahahahahahah',
			'user_password': 'huhuhuhuhuhu',
		})

		response = client.get(reverse('lab-9:cookie_profile'))
		self.assertTemplateUsed(response, 'lab_9/cookie/login.html')

	def test_lab_9_cookie_clear(self):
		client = Client()
		set_logged_in_cookie(client)
		response = client.get(reverse('lab-9:cookie_clear'))
		self.assertFalse(response.client.cookies['user_login'].value)
		self.assertFalse(response.client.cookies['user_password'].value)

##################  api_enterkomputer
	def test_lab_9_get_drones(self):
		drones = api_enterkomputer.get_drones()
		self.assertTrue(drones is not None)

	def test_lab_9_get_soundcards(self):
		soundcards = api_enterkomputer.get_soundcard()
		self.assertTrue(soundcards is not None)

	def test_lab_9_get_opticals(self):
		opticals = api_enterkomputer.get_optical()
		self.assertTrue(opticals is not None)

################# csui_helper
	def test_lab_9_csui_helper_get_access_token_success(self):
		access_token = csui_helper.get_access_token('test','test')
		self.assertTrue(access_token is not None)

	def test_lab_9_csui_helper_get_access_token_fail(self):
		patcher_request_error = patch('requests.request', side_effect=mocked_fail_access_token_request)
		with patcher_request_error:
			access_token = csui_helper.get_access_token('test','test')
			self.assertTrue(access_token is None)

	def test_lab_9_csui_helper_verify_user(self):
		user_data = csui_helper.verify_user('test')
		self.assertTrue(user_data is not None)

############### custom_auth
	def test_lab_9_custom_auth_login_success(self):
		client = Client()
		parameters = {
			'username':'test',
			'password':'test'
		}
		response = client.post(reverse('lab-9:auth_login'), parameters)
		self.assertTrue('user_login' in client.session)

	def test_lab_9_custom_auth_login_fail(self):
		patcher_custom_auth_get_access_token = patch('lab_9.custom_auth.get_access_token', return_value=None)
		client = Client()
		with patcher_custom_auth_get_access_token:
			parameters = {
				'username':'test',
				'password':'test'
			}
			response = client.post(reverse('lab-9:auth_login'), parameters)
			self.assertRedirects(response, reverse('lab-9:index'))

	def test_lab_9_logout_success(self):
		client = Client()
		set_logged_in_session(client.session)
		response = client.get(reverse('lab-9:auth_logout'))
		self.assertTrue('user_login' not in client.session)
